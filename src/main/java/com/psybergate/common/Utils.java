package com.psybergate.common;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Utils {

	public static Collection<Object> getInstancesFromProperties(String propertiesFile) {
		try {
			Properties props = new Properties();
			InputStream propsFileStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(propertiesFile);
			props.load(propsFileStream);
			propsFileStream.close();

			Collection<Object> instances = new ArrayList<Object>();
			for (Object object : props.values()) {
				String className = (String) object;
				instances.add(Class.forName(className).newInstance());
			}

			return instances;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static Collection<String> getValuesFromPropertiesFile(String propertiesFile) {
		try {
			Properties props = new Properties();
			InputStream propsFileStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(propertiesFile);
			props.load(propsFileStream);
			propsFileStream.close();

			Collection<String> values = new ArrayList<String>();
			for (Object object : props.values()) {
				values.add((String) object);
			}
			return values;
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

	public static Map<String, Object[]> loadControllersAndHandlers(String propertiesFile, Class<?>[] handlerParams) {
		try {
			Properties props = new Properties();
			InputStream propsFileStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(propertiesFile);
			props.load(propsFileStream);
			propsFileStream.close();

			Map<String, Object[]> controllers = new HashMap<String, Object[]>();

			for (Map.Entry<Object, Object> controllerEntry : props.entrySet()) {
				String pathInfo = ((String) controllerEntry.getKey()).toLowerCase();
				String handlerQualifiedName = (String) controllerEntry.getValue();
				int delimiterIndex = handlerQualifiedName.indexOf('#');
				String controllerName = handlerQualifiedName.substring(0, delimiterIndex);
				String methodName = handlerQualifiedName.substring(delimiterIndex + 1);

				Class<?> controllerClazz = Class.forName(controllerName);
				Method requestHandler = controllerClazz.getDeclaredMethod(methodName, handlerParams);
				requestHandler.setAccessible(true);
				controllers.put("/" + pathInfo, new Object[] { controllerClazz.newInstance(), requestHandler });
			}

			return controllers;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
