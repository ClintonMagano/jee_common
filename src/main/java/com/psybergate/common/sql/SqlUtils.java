package com.psybergate.common.sql;

import java.util.List;
import java.util.Map;

public class SqlUtils {

	private static final int COLUMN_NAME = 0;

	private static final int TYPE = 1;

	private static final int IS_PRIMARY_KEY = 2;

	private static final int IS_NULLABLE = 3;

	/**
	 * Writes the SQL query to persist the data specified in the <code>columnEntries</code> map to the
	 * table named <code>tableName</code> of some database.
	 * 
	 * @param tableName     The name of the table in the database
	 * @param columnEntries A map where the key is the name of the column, and the value is the value to
	 *                      be persisted for that column.
	 * @return A String with the actual sql query to be executed to insert data into a database table.
	 */
	public static String writeInsertQuery(String tableName, Map<String, Object> columnEntries) {
		String columns = "";
		String values = "";
		for (Map.Entry<String, Object> columnEntry : columnEntries.entrySet()) {
			columns += "\"" + columnEntry.getKey() + "\",";
			values += "'" + columnEntry.getValue() + "',";
		}
		columns = columns.substring(0, columns.length() - 1);
		values = values.substring(0, values.length() - 1);

		return String.format("INSERT INTO \"%s\" (%s) VALUES (%s);", tableName, columns, values);
	}

	/**
	 * Writes the SQL query to create a new database table with the name<code>tableName</code>, and with
	 * column names and properties specified by the <code>columnProperties</code> list.
	 * <p>
	 * The format of each entry of <code>columnProperties</code> is an Object array if length 4 with
	 * this information at each index:<br>
	 * {Name of the column, database type of data for this column, isPrimaryKey, isNullable}
	 * 
	 * @param tableName
	 * @param columnProperties
	 * @return A string with the SQL query to create a database table.
	 */
	public static String writeCreateQuery(String tableName, List<Object[]> columnProperties) {
		// Generate SQL to create the table.
		String ddlQuery = "CREATE TABLE " + "\"" + tableName + "\" (";
		for (Object[] objects : columnProperties) {
			String columnName = "\"" + ((String) objects[COLUMN_NAME]) + "\"";

			String type = " INT ";
			if (objects[TYPE].equals(String.class)) {
				type = " TEXT ";
			}

			String isPrimaryKey = "";
			if ((boolean) objects[IS_PRIMARY_KEY]) {
				isPrimaryKey = "PRIMARY KEY ";
			}

			String nullable = "";
			if (!((boolean) objects[IS_NULLABLE])) {
				nullable = "NOT NULL";
			}

			ddlQuery += columnName + type + isPrimaryKey + nullable + ", ";
		}

		ddlQuery = ddlQuery.substring(0, ddlQuery.length() - 2) + ");";

		return ddlQuery;
	}

	/**
	 * Writes the SQL query to select entries from a specific table where the value in the column
	 * specified by <code>columnName</code> equals the given <code>value</code>.
	 * 
	 * @param tableName  The name of the table in the database from which data is requested.
	 * @param columnName Column name where a condition will be checked.
	 * @param value      The value that selected entries must have for the specified columnName.
	 * @return A string with the SQL query to select data from a database table.
	 */
	public static String writeSelectAllWhereQuery(String tableName, String columnName, String value) {
		String query = String.format("SELECT * FROM \"%s\" WHERE \"%s\"='%s';", tableName, columnName, value);
		return query;
	}

	/**
	 * Writes the SQL query to delete an entry from a specific table where the value in the column
	 * specified by index 0 of the <code>columnIdentifier</code> array equals the value specified by
	 * index 1 of the <code>columnIdentifier</code> array.
	 * <p>
	 * Writes a SQL statement of the form: DEETE FROM tableName WHERE column=value
	 * 
	 * @param tableName        The table from which to delete an entry
	 * @param columnIdentifier The column name and value to be used in the equality condition of the
	 *                         "WHERE" part of a SQL DELETE query.
	 * @return The SQL query that will delete an entry from the specified table.
	 */
	public static String writeDeleteQuery(String tableName, Object[] columnIdentifier) {
		String query =
				String.format("DELETE FROM \"%s\" WHERE \"%s\"='%s';", tableName, columnIdentifier[0], columnIdentifier[1]);
		return query;
	}
}
