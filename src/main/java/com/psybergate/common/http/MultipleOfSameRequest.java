package com.psybergate.common.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class MultipleOfSameRequest {

	public static void main(String[] args) {
		int threadCount = 50;

		String request = "http://localhost:8080/enterprise_hwent3/v1/register";
		String requestMethod = "POST";
		Map<String, String> requestParameters = new HashMap<String, String>();
		requestParameters.put("customerNum", "4");
		requestParameters.put("name", "Clinton");
		requestParameters.put("surname", "Magano");
		requestParameters.put("dob", "1992-09-26");

		Thread[] threads = createHttpRequestThreads(threadCount, request, requestMethod, requestParameters);
		startThreads(threads);

		System.out.println(threadCount + " threads started");
	}

	private static void startThreads(Thread[] threads) {
		try {
			for (int i = 0; i < threads.length; i++) {
				threads[i].start();
			}
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static Thread[] createHttpRequestThreads(int i, String request, String requestMethod,
			Map<String, String> requestParameters) {
		Runnable[] runnables = new Runnable[i];
		Thread[] threads = new Thread[i];

		for (int j = 0; j < i; j++) {
			// Need an effectively final variable that will be used in the Runnable anonymous class.
			int runnableNumber = j;
			runnables[j] = new Runnable() {

				@Override
				public void run() {
					String response = "";
					try {
						response = makeHttpGetRequest(request, requestMethod, requestParameters);
					}
					catch (IOException e) {
						throw new RuntimeException(e);
					}

					System.out.println("Thread " + Thread.currentThread().getName() + " has responded with:\n\t\t" + response);
				}
			};

			threads[j] = new Thread(runnables[j]);
			threads[j].setName("" + runnableNumber);
		}

		return threads;
	}

	/**
	 * Sends a request to the specified url the same way a browser would, and
	 * returns the character data of the response as a String. The response excludes any headers.
	 * 
	 * @param requestUrl the url you would type in a browser.
	 * @param method     the HTTP request method to be used.
	 * @param parameters key-value pairs to be sent to the server, such as form data.
	 * @return The response from the server as a string. Contains only the
	 *         resource as a string, so no request headers are included.
	 * @throws IOException
	 */
	private static String makeHttpGetRequest(String requestUrl, String method, Map<String, String> parameters)
			throws IOException {
		URL url = new URL(requestUrl);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod(method);

		writeParametersForRequest(con, parameters);

		// Send request to browser.
		con.connect();

		// Closes the connection once the response is received.
		String response = getResponse(con);

		return response;
	}

	private static String getResponse(HttpURLConnection con) throws IOException {
		InputStreamReader inputStreamReader = new InputStreamReader(con.getInputStream());
		BufferedReader reader = new BufferedReader(inputStreamReader);
		StringBuilder response = new StringBuilder();
		String line;
		while ((line = reader.readLine()) != null) {
			response.append(line);
		}

		inputStreamReader.close();
		reader.close();
		con.disconnect();

		return response.toString();
	}

	private static void writeParametersForRequest(HttpURLConnection con, Map<String, String> parameters)
			throws IOException {
		// We intend to use the URLConnection for to write data to be used by server, usually data in a
		// form.
		con.setDoOutput(true);
		OutputStreamWriter outputStreamWriter = new OutputStreamWriter(con.getOutputStream());

		for (Map.Entry<String, String> parameter : parameters.entrySet()) {
			String keyAndValueText = String.format("%s=%s&", parameter.getKey(), parameter.getValue());
			outputStreamWriter.write(keyAndValueText);
		}

		outputStreamWriter.flush();
		outputStreamWriter.close();
	}
}
