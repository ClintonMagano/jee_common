package com.psybergate.common.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ProgrammaticHttpRequest {

	public static void main(String[] args) {
		Set<TestRequest> requests = new HashSet<TestRequest>();

		requests.add(new TestRequest("http://localhost:8080/enterprise_hwent4/v1/register", "POST", "customerNum=3",
				"name=Clinton", "surname=Magano", "dob=1992-09-26"));

		requests.add(new TestRequest("http://localhost:8080/enterprise_hwent4/v1/register", "POST", "customerNum=98",
				"name=Bob", "surname=Bigbum", "dob=1997-11-02"));

		requests.add(new TestRequest("http://localhost:8080/enterprise_hwent4/v1/register", "POST", "customerNum=102",
				"name=Sally", "surname=Sandshrew", "dob=2000-09-26"));

		requests.add(new TestRequest("http://localhost:8080/enterprise_hwent4/v1/register", "POST", "customerNum=3",
				"name=Mark", "surname=Mathews", "dob=2010-09-26"));

		Set<Thread> threads = createHttpRequestThreads(requests);
		startThreads(threads);

		System.out.println(threads.size() + " threads started\n");
	}

	private static void startThreads(Set<Thread> threads) {
		try {
			for (Thread thread : threads) {
				thread.start();
			}
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static Set<Thread> createHttpRequestThreads(Set<TestRequest> requests) {
		int runnableNumber = 0;

		Set<Thread> threads = new HashSet<Thread>();

		for (TestRequest testRequest : requests) {
			// Need an effectively final variable that will be used in the Runnable anonymous class.
			runnableNumber++;

			Runnable runnable = new Runnable() {

				@Override
				public void run() {
					String response = "";
					try {
						String requestUrl = testRequest.getRequestUrl();
						String requestMethod = testRequest.getRequestMethod();
						Map<String, String> requestParameters = testRequest.getRequestParameters();
						response = makeHttpGetRequest(requestUrl, requestMethod, requestParameters);
					}
					catch (IOException e) {
						throw new RuntimeException(e);
					}

					System.out.println("Thread " + Thread.currentThread().getName() + " has responded with:\n\t\t" + response);
				}
			};

			Thread thread = new Thread(runnable);
			thread.setName("" + runnableNumber);
			threads.add(thread);
		}

		return threads;
	}

	/**
	 * Sends a request to the specified url the same way a browser would, and
	 * returns the character data of the response as a String. The response excludes any headers.
	 * 
	 * @param requestUrl the url you would type in a browser.
	 * @param method     the HTTP request method to be used.
	 * @param parameters key-value pairs to be sent to the server, such as form data.
	 * @return The response from the server as a string. Contains only the
	 *         resource as a string, so no request headers are included.
	 * @throws IOException
	 */
	private static String makeHttpGetRequest(String requestUrl, String method, Map<String, String> parameters)
			throws IOException {
		URL url = new URL(requestUrl);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod(method);

		writeParametersForRequest(con, parameters);

		// Send request to browser.
		con.connect();

		// Closes the connection once the response is received.
		String response = getResponse(con);

		return response;
	}

	private static String getResponse(HttpURLConnection con) throws IOException {
		InputStreamReader inputStreamReader = new InputStreamReader(con.getInputStream());
		BufferedReader reader = new BufferedReader(inputStreamReader);
		StringBuilder response = new StringBuilder();
		String line;
		while ((line = reader.readLine()) != null) {
			response.append(line);
		}

		inputStreamReader.close();
		reader.close();
		con.disconnect();

		return response.toString();
	}

	private static void writeParametersForRequest(HttpURLConnection con, Map<String, String> parameters)
			throws IOException {
		// We intend to use the URLConnection for to write data to be used by server, usually data in a
		// form.
		con.setDoOutput(true);
		OutputStreamWriter outputStreamWriter = new OutputStreamWriter(con.getOutputStream());

		for (Map.Entry<String, String> parameter : parameters.entrySet()) {
			String keyAndValueText = String.format("%s=%s&", parameter.getKey(), parameter.getValue());
			outputStreamWriter.write(keyAndValueText);
		}

		outputStreamWriter.flush();
		outputStreamWriter.close();
	}
}
