package com.psybergate.common.http;

import java.util.HashMap;
import java.util.Map;

public class TestRequest {

	private String requestUrl;

	private String requestMethod;

	private Map<String, String> requestParameters = new HashMap<String, String>();

	public TestRequest(String requestUrl, String requestMethod, String... requestParameters) {
		super();
		this.requestUrl = requestUrl;
		this.requestMethod = requestMethod;
		storeRequestParameters(requestParameters);
	}

	public String getRequestUrl() {
		return requestUrl;
	}

	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}

	public String getRequestMethod() {
		return requestMethod;
	}

	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	public Map<String, String> getRequestParameters() {
		return requestParameters;
	}

	public void setRequestParameters(Map<String, String> requestParameters) {
		this.requestParameters = requestParameters;
	}

	private void storeRequestParameters(String[] requestParameters) {
		for (String parameter : requestParameters) {
			String key = getKey(parameter);
			String value = getValue(parameter);
			this.requestParameters.put(key, value);
		}
	}

	private String getKey(String parameter) {
		int indexOfEqualityChar = parameter.indexOf('=');
		return parameter.substring(0, indexOfEqualityChar);
	}

	private String getValue(String parameter) {
		int indexOfEqualityChar = parameter.indexOf('=');
		return parameter.substring(indexOfEqualityChar + 1, parameter.length());
	}

}
