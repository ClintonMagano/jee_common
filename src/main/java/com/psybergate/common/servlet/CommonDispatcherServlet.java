package com.psybergate.common.servlet;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.psybergate.common.Utils;

public class CommonDispatcherServlet extends HttpServlet {

	public static final String HANDLER_MAPPINGS = "requesthandlers/handlers.properties";

	/**
	 * Format: {request: [controller instance, Method instance that handles the request]}
	 */
	public static Map<String, Object[]> controllers;

	@Override
	public void init() throws ServletException {
		super.init();
		controllers = Utils.loadControllersAndHandlers(HANDLER_MAPPINGS, new Class<?>[] { HttpServletRequest.class });
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

	public void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pathInfo = req.getPathInfo();

		if (pathInfo == null || pathInfo.equals("/")) {
			req.getRequestDispatcher("/index.jsp").forward(req, resp);
			return;
		}

		Object controller = getController(pathInfo);
		Method handler = getHandler(pathInfo);

		if (controller == null || handler == null) {
			// The superclass's doGet() constructs an error page with a "405: Method not allowed" message. This
			// method calls the superclass's doGet method.
			sendHttp405Error(req, resp);
			return;
		}

		try {
			String response = (String) handler.invoke(controller, req);
			resp.setContentType("text");
			resp.getWriter().println(response);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private Method getHandler(String pathInfo) {
		Object[] controllerAndMethod = controllers.get(pathInfo);
		if (controllerAndMethod == null) return null;
		return (Method) controllerAndMethod[1];
	}

	private Object getController(String pathInfo) {
		Object[] controllerAndMethod = controllers.get(pathInfo);
		if (controllerAndMethod == null) return null;
		return controllerAndMethod[0];
	}

	private void sendHttp405Error(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// The superclass's doGet() constructs an error page.
		super.doGet(req, resp);
	}

}
